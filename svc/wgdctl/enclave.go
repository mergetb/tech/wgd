package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/wgd/api/wgdcoord"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

func enclave(root *cobra.Command) {
	enclave := &cobra.Command{
		Use:   "enclave",
		Short: "edit wireguard enclaves",
	}
	root.AddCommand(enclave)

	create := &cobra.Command{
		Use:   "create",
		Short: "create enclaveid enclavenet wgdcoordendpoint wgdcoordcertpath gatewayendpoint gatewaycertpath",
		Args:  cobra.ExactArgs(6),
		Run: func(cmd *cobra.Command, args []string) {
			createEnclave(args)
		},
	}
	enclave.AddCommand(create)

	remove := &cobra.Command{
		Use:   "remove",
		Short: "remove enclaveid wgdcoordendpoint wgdcoordcertpath",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			deleteEnclave(args)
		},
	}
	enclave.AddCommand(remove)

	clientattach := &cobra.Command{
		Use:   "clientattach",
		Short: "clientattach enclaveid key wgdcoordendpoint wgdcoordcertpath",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			clientAttach(args)
		},
	}
	enclave.AddCommand(clientattach)

	clientdetach := &cobra.Command{
		Use:   "clientdetach",
		Short: "clientdetach enclaveid key enclaveaddr wgdcoordendpoint wgdcoordcertpath",
		Args:  cobra.ExactArgs(5),
		Run: func(cmd *cobra.Command, args []string) {
			clientDetach(args)
		},
	}
	enclave.AddCommand(clientdetach)
}

func createEnclave(args []string) {
	encid, encnet, wgdcep, wgdccertpath, gwep, gwcertpath := args[0], args[1], args[2], args[3], args[4], args[5]

	cert, err := ioutil.ReadFile(gwcertpath)
	if err != nil {
		log.Fatal(err)
		return
	}

	var resp *wgdcoord.CreateEnclaveResp
	err = WgdCoord(
		wgdcep,
		wgdccertpath,
		func(cli wgdcoord.WgdcoordClient) error {
			r, err := cli.CreateEnclave(
				context.Background(),
				&wgdcoord.CreateEnclaveReq{
					Enclaveid:       encid,
					Allowedips:      encnet,
					Gatewayendpoint: gwep,
					Gatewaycert:     string(cert),
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func deleteEnclave(args []string) {
	encid, ep, certpath := args[0], args[1], args[2]

	var resp *wgdcoord.DeleteEnclaveResp
	err := WgdCoord(
		ep,
		certpath,
		func(cli wgdcoord.WgdcoordClient) error {
			r, err := cli.DeleteEnclave(
				context.Background(),
				&wgdcoord.DeleteEnclaveReq{
					Enclaveid: encid,
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func clientAttach(args []string) {
	encid, key, ep, certpath := args[0], args[1], args[2], args[3]

	var resp *wgdcoord.ClientAttachResp
	err := WgdCoord(
		ep,
		certpath,
		func(cli wgdcoord.WgdcoordClient) error {
			r, err := cli.ClientAttach(
				context.Background(),
				&wgdcoord.ClientAttachReq{
					Enclaveid: encid,
					Publickey: key,
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func clientDetach(args []string) {
	encid, key, encaddr, ep, certpath := args[0], args[1], args[2], args[3], args[4]

	var resp *wgdcoord.ClientDetachResp
	err := WgdCoord(
		ep,
		certpath,
		func(cli wgdcoord.WgdcoordClient) error {
			r, err := cli.ClientDetach(
				context.Background(),
				&wgdcoord.ClientDetachReq{
					Enclaveid:      encid,
					Publickey:      key,
					Enclaveaddress: encaddr,
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}
