package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/wgd/api/wgd"
	"gopkg.in/yaml.v2"
)

func gateway(root *cobra.Command) {
	gateway := &cobra.Command{
		Use:   "gateway",
		Short: "modify wireguard gateway",
	}
	root.AddCommand(gateway)

	createiface := &cobra.Command{
		Use:   "createiface",
		Short: "createiface enclaveid namespace enclaveaddr wgdendpoint wgdcertpath",
		Args:  cobra.ExactArgs(5),
		Run: func(cmd *cobra.Command, args []string) {
			createIface(args)
		},
	}
	gateway.AddCommand(createiface)

	deleteiface := &cobra.Command{
		Use:   "deleteiface",
		Short: "deleteiface enclaveid wgdendpoint wgdcertpath",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			deleteIface(args)
		},
	}
	gateway.AddCommand(deleteiface)

	addpeer := &cobra.Command{
		Use:   "addpeer",
		Short: "addpeer enclaveid key allowedips wgdendpoint wgdcertpath",
		Args:  cobra.ExactArgs(5),
		Run: func(cmd *cobra.Command, args []string) {
			addPeer(args)
		},
	}
	gateway.AddCommand(addpeer)

	delpeer := &cobra.Command{
		Use:   "delpeer",
		Short: "delpeer enclaveid key wgdendpoint wgdcertpath",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			delPeer(args)
		},
	}
	gateway.AddCommand(delpeer)
}

func createIface(args []string) {
	encid, namespace, encaddr, ep, certpath := args[0], args[1], args[2], args[3], args[4]

	var resp *wgd.CreateGatewayInterfaceResp
	err := Wgd(
		ep,
		certpath,
		func(cli wgd.WgdClient) error {
			r, err := cli.CreateGatewayInterface(
				context.Background(),
				&wgd.CreateGatewayInterfaceReq{
					Enclaveid:  encid,
					Namespace:  namespace,
					Accessaddr: encaddr,
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func deleteIface(args []string) {
	encid, ep, certpath := args[0], args[1], args[2]

	var resp *wgd.DeleteInterfaceResp
	err := Wgd(
		ep,
		certpath,
		func(cli wgd.WgdClient) error {
			r, err := cli.DeleteInterface(
				context.Background(),
				&wgd.DeleteInterfaceReq{
					Namespace: &wgd.WgNs{
						Key:  encid,
						Code: wgd.WgNsCode_Namespace,
					},
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func addPeer(args []string) {
	encid, key, ips, ep, certpath := args[0], args[1], args[2], args[3], args[4]

	var resp *wgd.WgPeerResp
	err := Wgd(
		ep,
		certpath,
		func(cli wgd.WgdClient) error {
			r, err := cli.AddWgPeer(
				context.Background(),
				&wgd.WgPeerReq{
					Enclaveid: encid,
					Peer: &wgd.WgPeerConfig{
						Key:        key,
						Allowedips: ips,
					},
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func delPeer(args []string) {
	encid, key, ep, certpath := args[0], args[1], args[2], args[3]

	var resp *wgd.WgPeerResp
	err := Wgd(
		ep,
		certpath,
		func(cli wgd.WgdClient) error {
			r, err := cli.DelWgPeer(
				context.Background(),
				&wgd.WgPeerReq{
					Enclaveid: encid,
					Peer: &wgd.WgPeerConfig{
						Key: key,
					},
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}
