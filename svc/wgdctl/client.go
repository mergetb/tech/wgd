package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/wgd/api/wgdcoord"
	"gopkg.in/yaml.v2"
)

func client(root *cobra.Command) {
	client := &cobra.Command{
		Use:   "client",
		Short: "run client commands",
	}
	root.AddCommand(client)

	create := &cobra.Command{
		Use:   "create",
		Short: "create enclaveid containername wgdcoordendpoint wgdcoordcertpath",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			clientContainerCreate(args)
		},
	}
	client.AddCommand(create)

	remove := &cobra.Command{
		Use:   "remove",
		Short: "remove enclaveid containername wgdcoordendpoint wgdcoordcertpath",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			clientContainerRemove(args)
		},
	}
	client.AddCommand(remove)
}

func clientContainerCreate(args []string) {
	encid, containername, wgdcep, wgdccertpath := args[0], args[1], args[2], args[3]

	var resp *wgdcoord.ClientContainerCreateResp
	err := WgdCoord(
		wgdcep,
		wgdccertpath,
		func(cli wgdcoord.WgdcoordClient) error {
			r, err := cli.ClientContainerCreate(
				context.Background(),
				&wgdcoord.ClientContainerCreateReq{
					Enclaveid:     encid,
					Containername: containername,
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}

func clientContainerRemove(args []string) {
	encid, containername, wgdcep, wgdccertpath := args[0], args[1], args[2], args[3]

	var resp *wgdcoord.ClientContainerDeleteResp
	err := WgdCoord(
		wgdcep,
		wgdccertpath,
		func(cli wgdcoord.WgdcoordClient) error {
			r, err := cli.ClientContainerDelete(
				context.Background(),
				&wgdcoord.ClientContainerDeleteReq{
					Enclaveid:     encid,
					Containername: containername,
				},
			)
			if err != nil {
				return err
			}
			resp = r
			return nil
		},
	)
	if err != nil {
		log.Fatal(err)
		return
	}
	d, err := yaml.Marshal(&resp)
	if err != nil {
		log.Fatal("bad response")
		return
	}
	fmt.Printf("--- \n%s\n\n", string(d))
}
