package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/wgd/common"
)

func main() {
	root := &cobra.Command{
		Use:   "wgdctl [wgcid]",
		Short: "CLI for controlling wgd & wgdcoord",
		Long:  "A command line utility to talk to wgdcoord & wgd",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) {},
	}
	rootCmds(root)
	enclave(root)
	gateway(root)
	client(root)
	root.Execute()
}

func rootCmds(root *cobra.Command) {

	version := &cobra.Command{
		Use:   "version",
		Short: "Get Merge version this utility was built against",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(common.Version)
		},
	}
	root.AddCommand(version)

}
