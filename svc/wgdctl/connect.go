package main

import (
	// "crypto/tls"
	// "crypto/x509"
	"fmt"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/tech/wgd/api/wgd"
	"gitlab.com/mergetb/tech/wgd/api/wgdcoord"
)

func WgdCoordClient(endpoint, certpath string) (*grpc.ClientConn, wgdcoord.WgdcoordClient, error) {

	creds, err := credentials.NewClientTLSFromFile(certpath, "")
	if err != nil {
		log.WithError(err).Error("tls client creds")
		return nil, nil, err
	}
	conn, err := grpc.Dial(endpoint, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.WithError(err).Error("wgdcoord connection failure")
		return nil, nil, fmt.Errorf("wgdcoord connection error")
	}
	return conn, wgdcoord.NewWgdcoordClient(conn), nil
}

// WgdCoord executes a function in the context of a wgdcoord grpc connection.
func WgdCoord(addr, certpath string, f func(wgdcoord.WgdcoordClient) error) error {
	conn, wgdc, err := WgdCoordClient(addr, certpath)
	defer conn.Close()
	if err != nil {
		return err
	}
	return f(wgdc)
}

func WgdClient(endpoint, certpath string) (*grpc.ClientConn, wgd.WgdClient, error) {

	creds, err := credentials.NewClientTLSFromFile(certpath, "")
	if err != nil {
		log.WithError(err).Error("tls client creds")
		return nil, nil, err
	}
	conn, err := grpc.Dial(endpoint, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.WithError(err).Error("wgd connection failure")
		return nil, nil, fmt.Errorf("wgd connection error")
	}
	return conn, wgd.NewWgdClient(conn), nil
}

// Wgd executes a function in the context of a wgd grpc connection.
func Wgd(addr, certpath string, f func(wgd.WgdClient) error) error {
	conn, wgdc, err := WgdClient(addr, certpath)
	defer conn.Close()
	if err != nil {
		return err
	}
	return f(wgdc)
}
