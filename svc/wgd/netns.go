package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"path/filepath"
	"strings"
)

type NsCodeType uint8

// NOTE: These must have the same values as the wgd grpc API!
const (
	None NsCodeType = iota
	ContainerdId
	Named
)

func (c NsCodeType) String() string {
	codeMap := map[NsCodeType]string{
		None:         "None",
		Named:        "Named",
		ContainerdId: "ContainerdId",
	}
	return codeMap[c]
}

type NsParser struct {
	Code NsCodeType
	Key  string
}

func (nsp *NsParser) String() string {
	return fmt.Sprintf("%s (%s)", nsp.Code.String(), nsp.Key)
}

// ToNamespace parses the given data and returns a net namespace
func (nsp *NsParser) ToNamespace() (string, error) {
	log.Infof("nsp code: %d", nsp.Code)
	switch nsp.Code {
	case None:
		return "", nil
	case Named:
		return nsp.Key, nil
	case ContainerdId:
		log.Info("found containerid code")
		return cid2NSName(nsp.Key) // convert containerd to a net namespace
	default:
		return "", fmt.Errorf("bad ns code")
	}
}

// cid2NSName takes a container ID and returns the netns NAME
func cid2NSName(cid string) (string, error) {
	// We know we're using containerd, so we use ctr to get the net ns
	// example cmd: ctr -n k8s.io c info $CID
	// This will return much json about the container. Parse out the
	// namespace from that.

	log.Info("converting containerdid into namespace. id: " + cid)
	out, err := RunOutput("ctr -n k8s.io c info " + cid)
	if err != nil {
		log.Info("error running ctr")
		return "", err
	}

	// Output is JSON, so build a type that matches the JSON
	// we want. Use that to parse out the data.
	type netNamespaceJSON struct {
		Spec struct {
			Linux struct {
				Namespaces []struct {
					Type string
					Path string
				}
			}
		}
	}
	var nnj netNamespaceJSON
	err = json.Unmarshal(out, &nnj)
	if err != nil {
		log.Info("json unmarshal")
		return "", err
	}
	path := ""
	for _, ns := range nnj.Spec.Linux.Namespaces {
		if ns.Type == "network" {
			path = ns.Path
			break
		}
	}
	if path == "" {
		return "", fmt.Errorf("net ns not found for " + cid)
	}
	// path is of the form /proc/PID/ns/net. If we get the PID we can query iproute2
	// for the namespace name.
	log.Info("found ns path: " + path)
	tokens := strings.Split(path, string(filepath.Separator))
	if len(tokens) != 5 { // 5 as there's an "" token at the start.
		return "", fmt.Errorf("do not know how to parse ns path")
	}

	pid := tokens[2]
	out, err = RunOutput("ip netns identify " + pid)
	if err != nil {
		return "", err
	}

	return string(out), nil
}
