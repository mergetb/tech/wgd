package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"strings"
)

func DoCreateClientInterface(enclaveid, accessaddr, allowedips, gwendpoint, gwkey, nskey string, nscode uint8) (string, error) {

	// create an empty, but active wg interface. the "namespace" is a ContainerdId for clients. Bad, bad, bad.
	dev, err := createInterface(enclaveid, accessaddr, nskey, nscode)
	if err != nil {
		return "", err
	}

	// All the 172.30. network nonsense being hardcoded is bad. last minute updates.
	gwAllowed := []string{allowedips, "172.30.0.0/16"}

	peer := &WgPeer{}
	err = peer.Parse(gwkey, gwendpoint, "25", gwAllowed)
	if err != nil {
		return "", err
	}

	err = dev.AddPeer(peer)
	if err != nil {
		return "", err
	}

	// GTL TODO fix this! No harcoded values!
	RunCommand(dev.Dev.ns("ip route add 192.168.254.1 dev " + dev.Dev.Name))
	RunCommand(dev.Dev.ns("ip route add 172.30.0.0/16 via 192.168.254.1"))

	return string(dev.PublicKey), nil
}

// DoCreateGatewayInterface is idmepotent.
func DoCreateGatewayInterface(enclaveid, namespace, accessaddr string) (string, uint16, error) {

	l := log.WithFields(log.Fields{
		"enclaveid":  enclaveid,
		"accessaddr": accessaddr,
		"namespace":  namespace,
	})
	l.Info("create gateway interface")

	wgDev, err := deviceExists(namespace, uint8(Named))
	if err != nil {
		return "", 0, err
	}
	if wgDev != nil { // device already exists.
		l.Infof("found existing device %s", wgDev.Dev.Name)
		key, err := wgDev.GetPublicKey()
		if err != nil {
			return "", 0, err
		}
		port, err := wgDev.GetListenPort()
		if err != nil {
			return "", 0, err
		}

		// return existing data.
		return key, port, nil
	}

	// check if device was deleted and if so, recreate
	// TBD GTL - use existing config() code to recreate

	// device does not exist, so create it.
	wgDev, err = createInterface(enclaveid, accessaddr, namespace, uint8(Named))
	if err != nil {
		return "", 0, err
	}

	// now that the interface is up, grab the listen port from wg
	lport, err := wgDev.GetListenPort()
	if err != nil {
		return "", 0, err
	}

	RunCommand(wgDev.Dev.ns("ip route add 192.168.254.0/24 dev " + wgDev.Dev.Name))

	return string(wgDev.PublicKey), lport, nil
}

func deviceExists(namespace string, nscode uint8) (*WgDev, error) {
	netns, err := getNetns(namespace, nscode)
	if err != nil {
		return nil, err
	}
	names, err := GetWgDevNames(netns)
	if err != nil {
		return nil, err
	}
	if len(names) != 1 {
		return nil, nil
	}

	wgdev := &WgDev{
		Dev: &IfDev{
			Name:      names[0],
			Namespace: netns,
		},
	}
	return wgdev, nil
}

func getNetns(ns string, code uint8) (string, error) {

	// get the namespace for the if
	// (for now we assume the namepsace code is valid.)
	nsp := &NsParser{Code: NsCodeType(code), Key: ns}
	return nsp.ToNamespace()
}

func createInterface(enclaveid, accessaddr, namespace string, nscode uint8) (*WgDev, error) {
	l := log.WithFields(log.Fields{
		"enclaveid":  enclaveid,
		"accessaddr": accessaddr,
		"namespace":  namespace,
		"nscode":     nscode,
	})
	l.Info("create interface")

	ns, err := getNetns(namespace, nscode)
	if err != nil {
		l.Error("bad namespace")
		return nil, err
	}

	dev, err := DevName(ns)
	if err != nil {
		return nil, err
	}

	ifdev := &IfDev{
		Name: dev,
		Type: "wireguard",
	}
	// add the interface
	err = ifdev.Add()
	if err != nil {
		l.Error("ifdev.add")
		return nil, err
	}
	// Add the wg device to generate keys and a listen port.
	wgdev := &WgDev{Dev: ifdev}

	// apply the configuration to the wireguard link
	// it is important to do this before moving to a namespace so the listen port
	// will listen in the default namespace.
	err = wgdev.Configure()
	if err != nil {
		l.Errorf("wgdev configure: %v", err)
		return nil, err
	}
	// now up the interface.
	err = ifdev.Up()
	if err != nil {
		l.Errorf("if up: %v", err)
		return nil, err
	}

	err = ifdev.ToNamespace(ns)
	if err != nil {
		msg := fmt.Sprintf("error moving if %s to namespace %s: %+v", ifdev.Name, ns, err)
		l.Info(msg)
		return nil, fmt.Errorf(msg)
	}

	// moving IF to a namespace removes the addr and UP.
	err = ifdev.Up()
	if err != nil {
		l.Errorf("if up: %v", err)
		return nil, err
	}

	err = ifdev.AddAddress(accessaddr)
	if err != nil {
		l.Errorf("add addr: %v", err)
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	return wgdev, nil
}

// DoDeleteInterface removes the wg interfaces from the local machine.
func DoDeleteInterface(namespace string, nscode uint8) (string, error) {

	l := log.WithFields(log.Fields{
		"namespace": namespace,
		"nscode":    nscode,
	})
	l.Info("delete interface")

	netns, err := getNetns(namespace, nscode)
	if err != nil {
		return "", err
	}
	names, err := GetWgDevNames(netns)
	if err != nil {
		return "", err
	}

	// There should be only one dev, but we're trying to be bulletproof here.
	var key string
	for _, name := range names {

		wgdev := &WgDev{
			Dev: &IfDev{
				Name:      name,
				Namespace: netns,
			},
		}

		key, err = wgdev.GetPublicKey()
		if err != nil {
			return "", err
		}

		// GTL TODO fix this! No harcoded values!
		RunCommand(wgdev.Dev.ns("ip route del 172.30.0.0/16 via 192.168.254.1"))
		RunCommand(wgdev.Dev.ns("ip route del 192.168.254.1 dev " + wgdev.Dev.Name))

		err = wgdev.Dev.Delete()
		if err != nil {
			log.WithFields(log.Fields{
				"name":      wgdev.Dev.Name,
				"namespace": wgdev.Dev.Namespace,
			}).Errorf("unable to remove device: %v", err)
			return "", err
		}
	}

	// return the last key found.
	return key, nil
}

func DoAddWgPeer(enclaveid, key, ep, allowedips string) (bool, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"key":       key,
		"endpoint":  ep,
	})
	l.Info("DoAddWgPeer")

	names, err := GetWgDevNames(enclaveid) // enclaveid == netns in this instance.
	if err != nil {
		return false, err
	}

	for _, name := range names {
		wgdev := &WgDev{
			Dev: &IfDev{
				Name:      name,
				Namespace: enclaveid,
			},
		}

		peer := &WgPeer{}
		err = peer.Parse(key, ep, "25", []string{allowedips})
		if err != nil {
			return false, err
		}

		err = wgdev.AddPeer(peer)
		if err != nil {
			return false, err
		}
	}
	return true, nil
}

func DoDelWgPeer(enclaveid, key string) (bool, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"key":       key,
	})
	l.Info("DoDelWgPeer")

	names, err := GetWgDevNames(enclaveid) // enclaveid == netns in this instance.
	if err != nil {
		return false, err
	}

	for _, name := range names {
		wgdev := &WgDev{
			Dev: &IfDev{
				Name:      name,
				Namespace: enclaveid,
			},
		}

		ok, err := wgdev.DelPeer(key)
		if err != nil {
			return false, err
		}
		if !ok {
			return false, fmt.Errorf("error deleting peer from %s", name)
		}
	}

	return true, nil
}

func DoInterfaceStatus(enclaveid, namespace string, nscode uint8) (string, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"namespace": namespace,
		"nscode":    nscode,
	})
	l.Info("interface status")

	netns, err := getNetns(namespace, nscode)
	if err != nil {
		l.Infof("namespace parse error: %v", err)
		return "", err
	}
	l.Infof("using namespace: %s", netns)
	var status []byte
	if netns != "" {
		status, err = RunOutput("ip netns exec " + netns + " wg")
	} else {
		status, err = RunOutput("wg")
	}

	if err != nil {
		l.Errorf("error: %+v", err)
		return "", err
	}
	return string(status), nil
}

// GetWgDevNames assumes the wg interface is active and in the namespace.
func GetWgDevNames(ns string) ([]string, error) {
	var out []byte
	var err error
	if ns == "" {
		out, err = RunOutput("wg show interfaces")
	} else {
		out, err = RunOutput("ip netns exec " + ns + " wg show interfaces")
	}
	if err != nil {
		return nil, err
	}
	return strings.Fields(string(out)), nil
}

func DevName(namespace string) (string, error) {
	// Did a bit of work trolling through namespaces calling 'ip link...'
	// and just realized using random names is much less error prone.
	tries := 0
	var err error
	for tries < 10 {
		name := randName()
		err = RunCommand("ip -br link show " + name)
		if err == nil {
			// hit a name in use try again
			tries++
			continue
		}
		// check namespace as well. (assumes namespace exists).
		if namespace != "" {
			err = RunCommand("ip netns exec " + namespace + " ip -br link show " + name)
			if err == nil {
				tries++
				continue
			}
		}
		return name, nil
	}
	return "", fmt.Errorf("unable to find valid device name")
}

func randName() string {
	const size = 4
	const letters = "abcedfghijklmnopqrstuvwxyz"
	s := make([]byte, size)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return "wgd" + string(s)
}
