package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

//
// WGD uses the wg showconf/setconf functionality to store wg configuration.
//

type WgdConfig struct {
	ConfPath string
}

func (wc *WgdConfig) SaveConfig(dev *WgDev) error {

	var path string
	if dev.Dev.Namespace == "" {
		path = filepath.Join(wc.ConfPath, "default")
	} else {
		path = filepath.Join(wc.ConfPath, dev.Dev.Namespace)
	}

	err := os.MkdirAll(path, 0755)
	if err != nil {
		return err
	}

	conf, err := dev.GetConfig()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filepath.Join(path, "wg.cfg"), conf, 0644)
	if err != nil {
		return err
	}

	conf, err = dev.Dev.GetConfig()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filepath.Join(path, "if.cfg"), conf, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (wc *WgdConfig) LoadConfig(namespace, devname string) error {

	path := filepath.Join(wc.ConfPath, namespace, devname)
	file := filepath.Join(path, "if.cfg")

	conf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil
	}

	ifdev := &IfDev{}

	// IfDef LoadConfig just reads in the configuration.
	err = ifdev.LoadConfig(conf)
	if err != nil {
		return err
	}

	err = ifdev.Add()
	if err != nil {
		return err
	}

	err = ifdev.Up()
	if err != nil {
		return err
	}

	// WgDev LoadConfig actually loads the configutration into the interface so there
	// is no need to Configure() it or read the bytes from the file.
	file = filepath.Join(path, "wg.cfg")
	wgdev := &WgDev{Dev: ifdev}
	err = wgdev.LoadConfig(file)
	if err != nil {
		return err
	}

	return nil
}
