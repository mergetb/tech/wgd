package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// WgDevError specific error type
type WgDevError error

// WgKey ...
type WgKey []byte // we know this is [32]byte, but since we get the key from stdout...

// WgDev ...
type WgDev struct {
	Dev        *IfDev
	ListenPort uint16
	PrivateKey WgKey
	PublicKey  WgKey
	Peers      []WgPeer
}

// WireguardEndpoint is just a small wrapper around net.UDPAddr fopr parsing.
type WgEndpoint net.UDPAddr

func WgEndpointParse(endpoint string) (*WgEndpoint, error) {
	host, port, err := net.SplitHostPort(endpoint)
	if err != nil {
		return nil, fmt.Errorf("bad endpoint: %s", endpoint)
	}

	ip := net.ParseIP(host)
	if ip == nil {
		// may be a net name?
		ipaddr, err := net.ResolveIPAddr("ip", host)
		if err != nil {
			return nil, fmt.Errorf("bad ip address. Host: %s", host)
		}
		ip = ipaddr.IP
	}
	log.Infof("using wg endpoint ip: %s", ip)

	intport, err := strconv.Atoi(port)
	if err != nil {
		return nil, fmt.Errorf("bad port")
	}
	return &WgEndpoint{Port: intport, IP: ip}, nil
}

func (wgep *WgEndpoint) String() string {
	u := net.UDPAddr{
		IP:   wgep.IP,
		Port: wgep.Port,
	}
	return u.String()
}

// WgPeer ...
type WgPeer struct {
	PublicKey           WgKey
	Endpoint            *WgEndpoint
	PersistentKeepalive uint16
	AllowedIPs          []*net.IPNet
}

func (wgp *WgPeer) Parse(key, endpoint, keepalive string, allowedips []string) error {

	// allowedips
	for _, ips := range allowedips {
		_, netCIDR, err := net.ParseCIDR(ips)
		if err != nil {
			return err
		}
		wgp.AllowedIPs = append(wgp.AllowedIPs, netCIDR)
	}

	// endpoint
	if endpoint != "" {
		ep, err := WgEndpointParse(endpoint)
		if err != nil {
			return err
		}
		wgp.Endpoint = ep
	}

	// key
	wgp.PublicKey = []byte(key)

	// keepalive
	ka, err := strconv.Atoi(keepalive)
	if err != nil {
		return err
	}
	if ka != 0 {
		wgp.PersistentKeepalive = uint16(ka)
	}

	return nil
}

// GenKeys generates keys for the given wireguard interface.
func (wg *WgDev) GenKeys() WgDevError {
	// generate our keys.
	log.Debug("generating keys for wireguard instance")

	var err error
	wg.PrivateKey, err = RunOutput(wg.Dev.ns("wg genkey"))
	if err != nil {
		log.WithError(err).Error("unble to generate private key")
		return err
	}

	wg.PublicKey, err = InCommandOutput(wg.PrivateKey, wg.Dev.ns("wg pubkey"))
	if err != nil {
		log.WithError(err).Error("unble to generate public key")
		return err
	}

	log.Debugf("keys: %s/%s", wg.PrivateKey, wg.PublicKey)

	return nil
}

// Configure the wg link using the given WgDev information.
func (wg *WgDev) Configure() WgDevError {
	cmd := "wg set " + wg.Dev.Name + " "
	if wg.ListenPort != 0 {
		err := RunCommand(wg.Dev.ns(cmd + "listen-port " + strconv.Itoa(int(wg.ListenPort))))
		if err != nil {
			return WgDevError(err)
		}
	}
	// private-key
	if len(wg.PrivateKey) == 0 {
		err := wg.GenKeys()
		if err != nil {
			return err
		}
	}
	privFile, err := ioutil.TempFile("", "wgpvt_")
	if err != nil {
		return err
	}
	defer os.Remove(privFile.Name())
	privFile.Write(wg.PrivateKey) // It would be nice if we could use an in-memory "File" here.
	err = RunCommand(wg.Dev.ns(cmd + " private-key " + privFile.Name()))
	if err != nil {
		return WgDevError(err)
	}

	// peers
	for _, peer := range wg.Peers {
		err = wg.AddPeer(&peer)
		if err != nil {
			return err
		}
	}

	return nil
}

// GetListenPort returns teh active listen port of the wg interface. Note the
// if must be up for this to exist unless it was set by hand before Configure().
func (wg *WgDev) GetListenPort() (uint16, error) {
	if wg.ListenPort != 0 {
		return wg.ListenPort, nil
	}
	out, err := RunOutput(wg.Dev.ns("wg show " + wg.Dev.Name + " listen-port"))
	if err != nil {
		return 0, nil
	}
	lp, err := strconv.Atoi(string(out))
	if err != nil {
		return 0, err
	}
	return uint16(lp), nil
}

func (wg *WgDev) GetPublicKey() (string, error) {
	if len(wg.PublicKey) != 0 {
		return string(wg.PublicKey), nil
	}
	out, err := RunOutput(wg.Dev.ns("wg show " + wg.Dev.Name + " public-key"))
	if err != nil {
		return "", err
	}
	return string(out), nil
}

func (wg *WgDev) AddPeer(peer *WgPeer) error {
	cmd := "wg set " + wg.Dev.Name + " peer " + string(peer.PublicKey)
	ips := " allowed-ips "
	for i, ip := range peer.AllowedIPs {
		ips += fmt.Sprint(ip)
		if i != len(peer.AllowedIPs)-1 {
			ips += ","
		}
	}
	cmd += ips
	if peer.Endpoint != nil {
		cmd += " endpoint " + peer.Endpoint.String()
	}
	if peer.PersistentKeepalive != 0 {
		pk := strconv.Itoa(int(peer.PersistentKeepalive))
		if pk != "" {
			cmd += " persistent-keepalive " + pk
		} else {
			return WgDevError(fmt.Errorf("bad keepalive"))
		}
	}
	err := RunCommand(wg.Dev.ns(cmd))
	if err != nil {
		return err
	}
	return nil
}

func (wg *WgDev) DelPeer(key string) (bool, error) {
	cmd := "wg set " + wg.Dev.Name + " peer " + key + " remove"
	err := RunCommand(wg.Dev.ns(cmd))
	if err != nil {
		return false, err
	}
	return true, nil
}

func (wg *WgDev) GetConfig() ([]byte, error) {
	out, err := RunOutput(wg.Dev.ns("wg showconf " + wg.Dev.Name))
	return out, err
}

// LoadConfig assumes the interface exists.
func (wg *WgDev) LoadConfig(file string) error {
	_, err := RunOutput(wg.Dev.ns("wg setconf " + wg.Dev.Name + " " + file))
	return err
}
