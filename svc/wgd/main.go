package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/wgd/common"
)

func main() {
	listen := flag.String("listen", "0.0.0.0:6000", "Where and how to listen as a daemon")
	cert := flag.String("cert", "/etc/wgd/wgd.pem", "Path to the wgd cert")
	key := flag.String("key", "/etc/wgd/wgd-key.pem", "Path to the wgd private key")
	verbose := flag.Bool("verbose", false, "If set, show debug logging.")
	version := flag.Bool("version", false, "Show version and exit.")
	flag.Parse()

	if *version == true {
		fmt.Println(common.Version)
		os.Exit(0)
	}

	if *verbose == true {
		log.SetLevel(log.DebugLevel)
	}

	rand.Seed(time.Now().UTC().UnixNano())

	err := Daemonize(*listen, *cert, *key)
	if err != nil {
		fmt.Println("Error starting daemon: ", err)
		os.Exit(666)
	}
	os.Exit(0)
}
