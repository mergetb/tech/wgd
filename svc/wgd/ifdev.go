package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"regexp"
)

// IfDevError wrap error in local error type
type IfDevError error

// IfDev encapsulates interface data.
type IfDev struct {
	Name      string
	Type      string
	Namespace string
	Address   string
}

// Add the interface to the system.
func (i *IfDev) Add() error {
	log.Debugf("adding interface %s", i.Name)
	err := RunCommand(i.ns("ip link add dev " + i.Name + " type " + i.Type))
	if err != nil {
		return err
	}
	if i.Address != "" {
		err = RunCommand(i.ns("ip addr add " + i.Address + " dev " + i.Name))
		if err != nil {
			return err
		}
	}
	return nil
}

// Delete the interface
func (i *IfDev) Delete() error {
	return RunCommand(i.ns("ip link delete dev " + i.Name))
}

// Up the interface
func (i *IfDev) Up() error {
	cmd := "ip link set mtu 1420 up dev " + i.Name
	return RunCommand(i.ns(cmd))
}

// ToNamespace - put the link in the given namespace
func (i *IfDev) ToNamespace(ns string) error {
	if ns == "" {
		return nil
	}
	i.Namespace = ns
	// Make sure the NS exists. We don't care if this fails.
	// RunCommand("ip netns add " + ns)
	return RunCommand("ip link set " + i.Name + " netns " + ns)
}

// DeleteNamespace kills the namspace of the link.
func (i *IfDev) DeleteNamespace() error {
	return RunCommand("ip netns del " + i.Namespace)
}

func (i *IfDev) GetAddress() (string, error) {
	if i.Address != "" {
		return i.Address, nil
	}
	cmd := "ip address show " + i.Name
	out, err := RunOutput(i.ns(cmd))
	if err != nil {
		return "", err
	}
	// Need to awkardly parse the address out.
	re := regexp.MustCompile(`[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}`)
	addr := re.FindString(string(out))
	if addr == "" {
		return "", fmt.Errorf("addr not set")
	}
	return addr, nil
}

func (i *IfDev) AddAddress(addr string) error {
	i.Address = addr
	err := RunCommand(i.ns("ip addr add " + i.Address + " dev " + i.Name))
	if err != nil {
		return err
	}
	return nil
}

func (i *IfDev) AddRoute(network string) error {
	// ip route add 172.30.0.0/16 dev wgfjan
	return RunCommand(i.ns("ip route add " + network + " dev " + i.Name))
}

func (i *IfDev) DelRoute(network string) error {
	// ip route del 172.30.0.0/16 dev wgfjan
	return RunCommand(i.ns("ip route del " + network + " dev " + i.Name))
}

// wrap the given command in a namespace exec space so if this
// link is in a namespace, we run the command in it.
func (i *IfDev) ns(c string) string {
	if i.Namespace != "" {
		return "ip netns exec " + i.Namespace + " " + c
	}
	return c
}

// GetConfig returns current ifdev configuration as a string. Use LoadConfig() to
// rebuild the interface.
func (i *IfDev) GetConfig() ([]byte, error) {
	conf, err := json.Marshal(i)
	if err != nil {
		return nil, err
	}
	return conf, nil
}

// Create an interface based on the configuration given.
func (i *IfDev) LoadConfig(conf []byte) error {
	return json.Unmarshal(conf, &i)
}
