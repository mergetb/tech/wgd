package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/wgd/api/wgd"
	"gitlab.com/mergetb/tech/wgd/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
)

//
// grpc boilerplate
//
// Server holds grpc functions.
type Server struct{}

func Daemonize(listenon, cert, key string) error {
	log.Infof("Starting wgd. Version %s", common.Version)

	creds, err := credentials.NewServerTLSFromFile(cert, key)
	if err != nil {
		log.Fatal("bad cert/key")
		return fmt.Errorf("bad cert/key")
	}

	grpcServer := grpc.NewServer(grpc.Creds(creds))
	wgd.RegisterWgdServer(grpcServer, &Server{})
	l, err := net.Listen("tcp", listenon)
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
		return err
	}
	log.Infof("listening on tcp://%s", listenon)
	grpcServer.Serve(l)
	return nil
}

// CreateClientWgIf ...
func (s *Server) CreateClientInterface(
	ctx context.Context, req *wgd.CreateClientInterfaceReq,
) (*wgd.CreateClientInterfaceResp, error) {

	l := log.WithFields(log.Fields{
		"enclaveid":    req.Enclaveid,
		"accessaddr":   req.Accessaddr,
		"nscode":       req.Namespace.Code,
		"nskey":        req.Namespace.Key,
		"gwallowedIPs": req.Gateway.Allowedips,
		"gwendpoint":   req.Gateway.Endpoint,
		"gwkey":        req.Gateway.Key,
	})
	l.Info("CreateClientWgIf")

	pubkey, err := DoCreateClientInterface(
		req.Enclaveid,
		req.Accessaddr,
		req.Gateway.Allowedips,
		req.Gateway.Endpoint,
		req.Gateway.Key,
		req.Namespace.Key,
		uint8(req.Namespace.Code),
	)
	if err != nil {
		l.Errorf("adding interface: %v", err)
		return nil, err
	}
	return &wgd.CreateClientInterfaceResp{
		Enclaveid: req.Enclaveid,
		Key:       pubkey,
	}, nil
}

// CreateServerWgIf ...
func (s *Server) CreateGatewayInterface(
	ctx context.Context, req *wgd.CreateGatewayInterfaceReq,
) (*wgd.CreateGatewayInterfaceResp, error) {

	l := log.WithFields(log.Fields{
		"enclaveid":  req.Enclaveid,
		"namespace":  req.Namespace,
		"accessaddr": req.Accessaddr,
	})
	l.Info("CreateGatewayInterface")

	key, port, err := DoCreateGatewayInterface(
		req.Enclaveid, req.Namespace, req.Accessaddr,
	)
	if err != nil {
		l.Errorf("creating server if")
		return nil, err
	}
	return &wgd.CreateGatewayInterfaceResp{
		Enclaveid:  req.Enclaveid,
		Publickey:  key,
		Listenport: uint32(port),
	}, nil
}

// DeleteInterface ...
func (s *Server) DeleteInterface(
	ctx context.Context, req *wgd.DeleteInterfaceReq,
) (*wgd.DeleteInterfaceResp, error) {

	l := log.WithFields(log.Fields{
		"namespace": req.Namespace.Key,
		"nscode":    req.Namespace.Code,
	})
	l.Info("DeleteInterface")

	key, err := DoDeleteInterface(req.Namespace.Key, uint8(req.Namespace.Code))
	if err != nil {
		l.Errorf("delete IF error %v", err)
	}

	return &wgd.DeleteInterfaceResp{
		Publickey: key,
	}, nil
}

// AddWgPeer ...
func (s *Server) AddWgPeer(
	ctx context.Context, req *wgd.WgPeerReq,
) (*wgd.WgPeerResp, error) {
	l := log.WithFields(log.Fields{
		"enclaveid":   req.Enclaveid,
		"key":         req.Peer.Key,
		"ep":          req.Peer.Endpoint,
		"allowed ips": req.Peer.Allowedips,
	})
	l.Info("Add WG Peer")

	resp, err := DoAddWgPeer(req.Enclaveid, req.Peer.Key, req.Peer.Endpoint, req.Peer.Allowedips)
	if err != nil {
		return nil, err
	}
	return &wgd.WgPeerResp{Ok: resp}, nil
}

// DelWgPeer ...
func (s *Server) DelWgPeer(
	ctx context.Context, req *wgd.WgPeerReq,
) (*wgd.WgPeerResp, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": req.Enclaveid,
		"key":       req.Peer.Key,
	})
	l.Info("Del WG Peer")

	resp, err := DoDelWgPeer(req.Enclaveid, req.Peer.Key)
	if err != nil {
		return nil, err
	}
	return &wgd.WgPeerResp{Ok: resp}, nil
}

func (s *Server) InterfaceStatus(
	ctx context.Context, req *wgd.InterfaceStatusReq,
) (*wgd.InterfaceStatusResp, error) {

	l := log.WithFields(log.Fields{
		"enclaveid": req.Enclaveid,
		"namespace": req.Namespace.Key,
		"nscode":    req.Namespace.Code,
	})
	l.Info("interface status")

	status, err := DoInterfaceStatus(req.Enclaveid, req.Namespace.Key, uint8(req.Namespace.Code))
	if err != nil {
		return nil, err
	}
	return &wgd.InterfaceStatusResp{
		Wgstatus: status,
	}, nil
}
