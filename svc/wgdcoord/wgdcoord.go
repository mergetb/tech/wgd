package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"regexp"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/engine/mfs"
	xdcd "gitlab.com/mergetb/portal/xdcd/api"

	"google.golang.org/grpc"

	"gitlab.com/mergetb/tech/wgd/api/wgd"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

//
// A "normal" wireguard connection daemon coordinator would scrounge up its own way
// to store connection states. Since this daemon is running in a Merge portal,
// we take advantage of the existing mfs/etcd service.
//

// ErrInternal ...
var ErrInternal = fmt.Errorf("internal error")
var ErrNotFound = fmt.Errorf("not found")

var ctx = context.Background()

func createEnclaveIfNeeded(encid string) error {
	wge := &WgEnclave{Enclaveid: encid}
	err := mfs.GetObject(wge)
	if err != nil {
		return err
	}
	return nil
}

// DoClientAttach ...
func DoClientAttach(enclaveid, key string) (string, string, string, string, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"key":       key,
	})
	l.Info("DoClientAttach")

	err := createEnclaveIfNeeded(enclaveid)
	if err != nil {
		return "", "", "", "", err
	}

	wge := &WgEnclave{Enclaveid: enclaveid}
	err = mfs.GetObject(wge)
	if err != nil {
		l.Errorf("internal error")
		return "", "", "", "", fmt.Errorf("internal error")
	}
	l.Info("creating/updating wgconnection ", enclaveid)
	wge.Merge(&WgEnclave{
		Enclaveid:  enclaveid,
		ClientKeys: []string{key},
	})
	_, err = mfs.PutObject(wge)
	if err != nil {
		l.Errorf("internal mfs error")
		// I don't really know what to do here.
		return "", "", "", "", err
	}

	// Grab the next available address for the client.
	addr, err := ReserveEnclaveAddress(enclaveid)
	if err != nil {
		return "", "", "", "", err
	}
	// claim the address immediately as we already have the key
	err = ClaimEnclaveAddress(enclaveid, addr, key)
	if err != nil {
		return "", "", "", "", err
	}

	// Tell the gateway about the new client.
	err = Wgd(
		wge.GatewayWgdEndpoint,
		wge.GatewayWgdCert,
		func(cli wgd.WgdClient) error {
			resp, err := cli.AddWgPeer(
				ctx,
				&wgd.WgPeerReq{
					Enclaveid: enclaveid,
					Peer: &wgd.WgPeerConfig{
						Key:        key,
						Allowedips: addr + "/32", // allowedips must be in CIDR format.
					},
				},
			)
			if err != nil {
				return err
			}
			if resp.Ok != true {
				return fmt.Errorf("failed to add client to gateway")
			}
			return nil
		},
	)
	if err != nil {
		return "", "", "", "", err
	}

	return wge.GatewayKey, wge.GatewayEndpoint, wge.AllowedIPs, addr, nil
}

// DoClientDetach ...
func DoClientDetach(enclaveid, key, enclaveaddr string) error {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"key":       key,
		"addr":      enclaveaddr,
	})
	l.Info("DoClientDetach")

	wge := &WgEnclave{Enclaveid: enclaveid}
	exists, err := mfs.ObjectExists(wge)
	if err != nil {
		l.Errorf("internal mfs error")
		return ErrInternal
	}
	if exists == false {
		// Not an error per se, but a misunderstanding somehow on a client's end?
		l.Errorf("wgd does not exist")
		return fmt.Errorf("connection does not exist")
	}
	// remove the client key.
	for i, k := range wge.ClientKeys {
		if k == key {
			wge.ClientKeys = append(wge.ClientKeys[:i], wge.ClientKeys[i+1:]...)
			break
		}
	}
	_, err = mfs.PutObject(wge)

	err = FreeEnclaveAddress(enclaveid, key)
	if err != nil {
		return err
	}

	// Tell the gateway that a client is gone.
	err = Wgd(
		wge.GatewayWgdEndpoint,
		wge.GatewayWgdCert,
		func(cli wgd.WgdClient) error {
			_, err := cli.DelWgPeer(
				ctx,
				&wgd.WgPeerReq{
					Enclaveid: enclaveid,
					Peer: &wgd.WgPeerConfig{
						Key: key,
					},
				},
			)
			if err != nil {
				return err
			}
			return nil
		},
	)

	return nil
}

// DoCreateEnclave just sets up local etcd information about the enclave, wgd certs, address space, etc.
func DoCreateEnclave(enclaveid, allowedips, gwendpoint, gwcert string) error {
	l := log.WithFields(log.Fields{
		"enclaveid":  enclaveid,
		"allowedips": allowedips,
		"gwendpoint": gwendpoint,
		"gwcert":     gwcert,
	})
	l.Info("create enclave")

	// setup the enclave address pool
	wga := &WgEnclaveAddress{Enclaveid: enclaveid}
	err := mfs.GetObject(wga)
	if err != nil {
		return err
	}
	mask := net.IPv4Mask(255, 255, 255, 0)                 // ffffff00 - GTL this should not be hardcoded.
	wga.Mask = [4]byte{mask[0], mask[1], mask[2], mask[3]} // better way to do this, I'm sure.
	ip, _, err := net.ParseCIDR(allowedips)
	ip = ip.To4()
	ip[2] = 254 // Again, should not be hardcoded. We take x.x.254.0/24 for wireguard addresses
	wga.Addr = [4]byte{ip[0], ip[1], ip[2], ip[3]}
	_, err = mfs.PutObject(wga)
	if err != nil {
		return err
	}

	// setup the enclave wireguard details.
	wge := &WgEnclave{Enclaveid: enclaveid}
	err = mfs.GetObject(wge)
	if err != nil {
		return err
	}

	// Grab the very first address for the gateway.
	gwAddr := net.IPv4(ip[0], ip[1], ip[2], 1).String()
	if err != nil {
		return err
	}

	// save details. Needed details are gw wgd endpoint port and gw wgd public key.
	// these will be gotten on demand at first attach
	wge.GatewayWgdCert = gwcert
	wge.GatewayWgdEndpoint = gwendpoint
	wge.GatewayAddress = gwAddr
	wge.AllowedIPs = allowedips
	wge.Enclaveid = enclaveid

	_, err = mfs.PutObject(wge)
	if err != nil {
		return err
	}

	return nil
}

// createGatewayInterface calls out to the wgd on sites to create the
// gateway interface. it gets the gw public key back and stores it
// in the etcd enclave data.
func createGatewayInterface(enclaveid string) error {

	// grab enclave details.
	wge := &WgEnclave{Enclaveid: enclaveid}
	err := mfs.GetObject(wge)
	if err != nil {
		return err
	}

	// Ask the wgd on the site to create a wg interface and give us back wg peer details.
	// We'll store those away and hand them out when clients attach to the enclave.
	err = Wgd(
		wge.GatewayWgdEndpoint,
		wge.GatewayWgdCert,
		func(cli wgd.WgdClient) error {
			resp, err := cli.CreateGatewayInterface(
				ctx,
				&wgd.CreateGatewayInterfaceReq{
					Enclaveid:  enclaveid,
					Namespace:  enclaveid, // tricky, tricky
					Accessaddr: wge.GatewayAddress,
				},
			)
			if err != nil {
				return err
			}
			// wg interface address is same as gateway address
			log.Infof("from GW - key: %s, port %d", resp.Publickey, resp.Listenport)
			gwaddr, _, _ := net.SplitHostPort(wge.GatewayWgdEndpoint)
			port := strconv.Itoa(int(resp.Listenport))
			wge.GatewayEndpoint = net.JoinHostPort(gwaddr, port)
			wge.GatewayKey = resp.Publickey
			return nil
		},
	)
	if err != nil {
		log.Infof("error creating wg interface: %+v", err)
		return err
	}

	// Store the possibly updated enclave data.
	_, err = mfs.PutObject(wge)
	if err != nil {
		return err
	}

	// claim the address as we now have the key
	err = ClaimEnclaveAddress(enclaveid, wge.GatewayAddress, wge.GatewayKey)
	if err != nil {
		return err
	}

	return nil
}

// DoDeleteEnclave ...
func DoDeleteEnclave(enclaveid string) (bool, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
	})
	l.Info("do delete enclave")

	wge := &WgEnclave{Enclaveid: enclaveid}
	err := mfs.GetObject(wge)
	if err != nil {
		l.Error("no such enclave")
		return false, err
	}

	wga := &WgEnclaveAddress{Enclaveid: enclaveid}
	mfs.DelObject(wga)

	if wge.GatewayEndpoint != "" {
		// Ask the wgd on the site to delete wg interface
		err := Wgd(
			wge.GatewayWgdEndpoint,
			wge.GatewayWgdCert,
			func(cli wgd.WgdClient) error {
				_, err := cli.DeleteInterface(
					ctx,
					&wgd.DeleteInterfaceReq{
						Namespace: &wgd.WgNs{
							Key:  enclaveid,
							Code: wgd.WgNsCode_Namespace,
						},
					},
				)
				return err
			},
		)
		if err != nil {
			return false, err
		}
	}

	// Tell attached clients to detach.
	err = detachClients(enclaveid)
	if err != nil {
		log.Errorf("detachClients: %s", err)
		return false, err
	}

	mfs.DelObject(wge)
	return true, nil
}

// ReserveEnclaveAddress returns the next available addresss, if any. Error otherwise.
// Should be folllowed at somepoint with a ClaimEnclaveAddress so we can free the address
// later.
func ReserveEnclaveAddress(enclaveid string) (string, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
	})
	l.Info("reserve enc address")

	wga := &WgEnclaveAddress{Enclaveid: enclaveid}
	exists, err := mfs.ObjectExists(wga)
	if err != nil {
		l.Errorf("internal mfs error")
		return "", ErrInternal
	}
	if exists == false {
		l.Error("enclave does not exist")
		return "", fmt.Errorf("enclave does not exist")
	}
	// find the next free address
	for i, inuse := range wga.InUse {
		if i == 0 || i == 1 { // skip x.x.x.0 and x.x.x.1
			continue
		}
		if inuse == false {
			// found one. mark it as used and save the state.
			wga.InUse[i] = true
			mfs.PutObject(wga)

			// now build the address.
			ip := net.IPv4(wga.Addr[0], wga.Addr[1], wga.Addr[2], wga.Addr[3])
			ip = ip.Mask(net.IPv4Mask(wga.Mask[0], wga.Mask[1], wga.Mask[2], wga.Mask[3])).To4()
			ip[3] = byte(i)
			return ip.String(), nil
		}
	}
	return "", fmt.Errorf("no available enclave addresses")
}

// ClaimEnclaveAddress marks a spefic address as reserved by a specific key.
// The address claimed, must have been reserved with ReserveEnclaveAddress()
func ClaimEnclaveAddress(enclaveid, address, key string) error {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"address":   address,
		"key":       key,
	})
	l.Info("claim enc address")

	ar := &AddressReservation{Enclaveid: enclaveid, ID: key}
	err := mfs.GetObject(ar)
	if err != nil {
		l.Errorf("internal mfs error")
		return ErrInternal
	}
	ar.Address = address
	mfs.PutObject(ar)
	return nil
}

// FreeEnclaveAddress is a hook to free an address. It currently does *NOT* free the address for use
// until wgdcoord can keep track of all clients' states, which may be never. So we err on the
// side of caution and hope that there will never be more than 254 clients for a given
// materialization.
func FreeEnclaveAddress(enclaveid, key string) error {
	l := log.WithFields(log.Fields{
		"enclaveid": enclaveid,
		"key":       key,
	})
	l.Info("free enc address")

	// ar := &AddressReservation{Enclaveid: enclaveid, ID: key}
	// err := mfs.GetObject(ar)
	// if err != nil {
	// 	l.Errorf("internal mfs error")
	// 	return ErrInternal
	// }
	// if ar.Address == "" {
	// 	l.Errorf("address is  not claimed")
	// 	return ErrInternal
	// }

	// addr := ar.Address
	// mfs.DelObject(ar)

	// wga := &WgEnclaveAddress{Enclaveid: enclaveid}
	// exists, err := mfs.ObjectExists(wga)
	// if err != nil {
	// 	l.Errorf("internal mfs error")
	// 	return ErrInternal
	// }
	// if exists == false {
	// 	l.Error("enclave does not exist")
	// 	return fmt.Errorf("enclave does not exist")
	// }
	// ip := net.ParseIP(addr).To4()
	// wga.InUse[ip[3]] = false
	// mfs.PutObject(wga)

	return nil
}

// DoClientContainerCreate assumes much: 1) this is a container in a merge portal,
// 2) k8s is managing the container, 3) containerd is the container interface,
// 4) a wgd instance is running on the container host and the endpoint is listening
//    on port 36000.
//
// This is not at all even close to a general "make me a wgif on my host" solution.
// Most likely, it doesn't even work well in a merge portal.
func DoClientContainerCreate(enclaveid, containername string) (string, string, error) {
	// Find the correct host for the container, and tell the wgd there
	// to create the client side interface.

	l := log.WithFields(log.Fields{
		"enclaveid":     enclaveid,
		"containername": containername,
	})
	l.Info("DoClientContainerCreate")

	// this is very merge/portal specific at the moment.
	nodeEp, contID, err := containerName2WgdData(containername)
	if err != nil {
		l.Errorf("error getting wgd info from container name: %+v", err)
		return "", "", fmt.Errorf("bad request: container name: %+v", err)
	}

	wge := &WgEnclave{Enclaveid: enclaveid}
	exists, err := mfs.ObjectExists(wge)
	if err != nil {
		l.Errorf("internal mfs error")
		return "", "", ErrInternal
	}
	if exists == false {
		l.Info("no wgd enclave for requested client")
		return "", "", fmt.Errorf("bad request")
	}

	// ask the gw to create the wg interface if it does not exist.
	err = createGatewayInterface(enclaveid)
	if err != nil {
		l.Infof("error creating gateway interface: %+v", err)
		return "", "", err
	}

	// re-grab the possibly new enclave data.
	err = mfs.GetObject(wge)
	if err != nil {
		return "", "", err
	}

	if wge.GatewayKey == "" {
		l.Info("no wgd server for requested client")
		return "", "", fmt.Errorf("bad request")
	}

	xdcWgdCert, err := ioutil.ReadFile(xdcCert)
	if err != nil {
		return "", "", err
	}

	enclaveaddr, err := ReserveEnclaveAddress(enclaveid)
	if err != nil {
		return "", "", nil
	}

	l.Infof("client container connecting to %s @ %s\n", wge.Enclaveid, enclaveaddr)
	l.Infof("wgdcoord contacting wgd on %s\n", nodeEp)

	// call out to the wgd on the worker node.
	var key string
	err = Wgd(
		nodeEp,
		string(xdcWgdCert),
		func(cli wgd.WgdClient) error {
			resp, err := cli.CreateClientInterface(
				context.Background(),
				&wgd.CreateClientInterfaceReq{
					Enclaveid:  enclaveid,
					Accessaddr: enclaveaddr,
					Namespace: &wgd.WgNs{
						Code: wgd.WgNsCode_ContainerdId,
						Key:  contID,
					},
					Gateway: &wgd.WgPeerConfig{
						Key:        wge.GatewayKey,
						Allowedips: wge.AllowedIPs,
						Endpoint:   wge.GatewayEndpoint,
					},
				},
			)
			if err != nil {
				l.Errorf("CreateClientWgIf error")
				return err
			}
			key = resp.Key
			return nil
		},
	)
	if err != nil {
		return "", "", err
	}

	// now that we have the key, claim the reserved address
	err = ClaimEnclaveAddress(enclaveid, enclaveaddr, key)
	if err != nil {
		l.Errorf("error claiming enc addr %s @ %s: %+v", enclaveid, enclaveaddr, err)
		return "", "", err
	}

	// Tell the wgd on the gateway about this new client.
	l.Infof("wgdcoord contacting wgd on site @ %s\n", wge.GatewayWgdEndpoint)
	err = Wgd(
		wge.GatewayWgdEndpoint,
		wge.GatewayWgdCert,
		func(cli wgd.WgdClient) error {
			_, err := cli.AddWgPeer(
				ctx,
				&wgd.WgPeerReq{
					Enclaveid: enclaveid,
					Peer: &wgd.WgPeerConfig{
						Key:        key,
						Allowedips: enclaveaddr + "/32", // allowedips must in CIDR format.
						// no endpoint for clients
					},
				},
			)
			if err != nil {
				l.Error("gw adding peer")
				return err
			}
			return nil
		},
	)
	if err != nil {
		l.Error("add peer on gw")
		return "", "", err
	}

	mfs.PutObject(
		&EnclaveClient{
			Client:    containername,
			Enclaveid: enclaveid,
		},
	)

	return enclaveid, enclaveaddr, nil
}

// DoClientContainerDelete ...
func DoClientContainerDelete(enclaveid, containername string) (string, error) {
	// Find the correct host for the container, and tell the wgd there
	// to create the client side interface.

	l := log.WithFields(log.Fields{
		"enclaveid":     enclaveid,
		"containername": containername,
	})
	l.Info("DoClientContainerDelete")

	// this is very merge/portal specific at the moment.
	nodeEp, contId, err := containerName2WgdData(containername)
	if err != nil {
		l.Error("error getting wgd info from container name")
		return "", fmt.Errorf("bad request: container name")
	}

	xdcWgdCert, err := ioutil.ReadFile(xdcCert)
	if err != nil {
		return "", err
	}

	// call out to the wgd on the worker node.
	var key string
	err = Wgd(
		nodeEp,
		string(xdcWgdCert),
		func(cli wgd.WgdClient) error {
			resp, err := cli.DeleteInterface(
				context.Background(),
				&wgd.DeleteInterfaceReq{
					Namespace: &wgd.WgNs{
						Key:  contId,
						Code: wgd.WgNsCode_ContainerdId,
					},
				},
			)
			if err != nil {
				l.Errorf("DeleteClientInterface")
				return err
			}
			key = resp.Publickey
			return nil
		},
	)
	if err != nil {
		return "", nil
	}

	// Free the address for re-use.
	FreeEnclaveAddress(enclaveid, key)

	// If we still have enclave data, tell the wgd on the site
	// to remove this peer. If we don't the enclave is probably
	// already de-mtz'd.
	wge := &WgEnclave{Enclaveid: enclaveid}
	exists, err := mfs.ObjectExists(wge)
	if err != nil {
		l.Errorf("internal mfs error")
		return "", ErrInternal
	}
	if exists == false {
		// Not really an error if the exp has been de-mtz'd.
		l.Info("no wgd enclave for requested client. not removing peer from site.")
		return "", nil
	}
	l.Infof("client container disconnecting from %v", wge)

	// Tell the gateway to remove this peer.
	err = Wgd(
		wge.GatewayWgdEndpoint,
		wge.GatewayWgdCert,
		func(cli wgd.WgdClient) error {
			_, err := cli.DelWgPeer(
				ctx,
				&wgd.WgPeerReq{
					Enclaveid: enclaveid,
					Peer: &wgd.WgPeerConfig{
						Key: key,
					},
				},
			)
			if err != nil {
				l.Error("gw removing peer")
				return err
			}
			return nil
		},
	)

	// Remove this client from etcd
	wgc := &EnclaveClient{Client: containername}
	exists, err = mfs.ObjectExists(wgc)
	if err != nil {
		return "", ErrInternal
	}
	if exists {
		mfs.DelObject(wgc)
	}

	return enclaveid, nil
}

func containerName2WgdData(name string) (string, string, error) {
	// from the containername, get the pod name. from the pod name, we can get the containerdid
	// and the containerdid is what wgd needs to find the container on the node side.
	// in merge/portal the container name is the pod name.
	l := log.WithFields(log.Fields{
		"name": name,
	})
	l.Info("containerName2WgdData")

	config, err := rest.InClusterConfig()
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to fetch kubeconfig")
	}

	k8c, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
		}).Fatal("failed to create k8s client")
		return "", "", err
	}
	// We only support names of the form xdc.eid.pid
	tokens := regexp.MustCompile("[.]").Split(name, 3)
	if len(tokens) != 3 {
		return "", "", fmt.Errorf("bad container name format")
	}
	xdcid, eid, pid := tokens[0], tokens[1], tokens[2]
	labSel := fmt.Sprintf("proj=%s, xp=%s, name=%s", pid, eid, xdcid)
	pods, err := k8c.CoreV1().Pods("xdc").List(
		v1.ListOptions{
			LabelSelector: labSel,
		},
	)
	if err != nil {
		l.Errorf("pod not found %s: %v", name, err)
		return "", "", err
	}
	if len(pods.Items) != 1 {
		l.Errorf("wrong number of pods found: %d", len(pods.Items))
		l.Errorf("label used: %s", labSel)
		return "", "", fmt.Errorf("pod not found")
	}
	pod := pods.Items[0]

	// GTL TODO this should be setup as a host service/nodePort and we can grab the endpoint from there,
	// specifically the port.
	// hostEP := pod.Spec.NodeName + ":36000"
	hostEP := pod.Status.HostIP + ":36000"

	// format is like "containerd://bb213810335cfe6f1494681..."
	contIDUrl := pod.Status.ContainerStatuses[0].ContainerID
	contID := strings.Split(contIDUrl, "//")[1]

	log.Info("found data - node ip: " + hostEP + " containerID: " + contID)

	return hostEP, contID, nil
}

func DoClientContainerStatus(name string) (bool, string, string, error) {
	l := log.WithFields(log.Fields{
		"containername": name,
	})
	l.Info("DoClientContainerStatus")

	// this is very merge/portal specific at the moment.
	nodeEp, contID, err := containerName2WgdData(name)
	if err != nil {
		l.Error("error getting wgd info from container name")
		return false, "", "", fmt.Errorf("bad request: container name")
	}

	cliid := &EnclaveClient{Client: name}
	exists, err := mfs.ObjectExists(cliid)
	if err != nil {
		l.Errorf("internal mfs error")
		return false, "", "", ErrInternal
	}
	if exists == false {
		l.Infof("no wgd enclave for client %s", name)
		return false, "", "", fmt.Errorf("bad request")
	}

	xdcWgdCert, err := ioutil.ReadFile(xdcCert)
	if err != nil {
		return false, "", "", err
	}

	var attached bool
	var encid, status string
	err = Wgd(
		nodeEp,
		string(xdcWgdCert),
		func(cli wgd.WgdClient) error {
			resp, err := cli.InterfaceStatus(
				context.Background(),
				&wgd.InterfaceStatusReq{
					Enclaveid: cliid.Enclaveid,
					Namespace: &wgd.WgNs{
						Code: wgd.WgNsCode_ContainerdId,
						Key:  contID,
					},
				},
			)
			if err != nil {
				l.Errorf("InterfaceStatus error")
				return err
			}
			attached = resp.Wgstatus != ""
			status = resp.Wgstatus
			encid = "TBD"
			return nil
		},
	)
	return attached, encid, status, nil
}

func detachClients(encid string) error {

	log.Infof("detaching enclave clients from %s", encid)

	dir, err := mfs.Lsl("/wgclient")
	if err != nil {
		return ErrInternal
	}

	for _, kv := range dir {

		c := &EnclaveClient{}
		mfs.FromJson(c, kv.Value)

		if c.Enclaveid == encid {
			// Delete the tunnel for this client.
			log.Infof("deleting wg interface on %s", c.Client)

			_, err := DoClientContainerDelete(c.Enclaveid, c.Client)
			if err != nil {
				log.Error("container %s delete: %s", c.Client, err)
			}

			err = clearTunnelData(c.Client)
			if err != nil {
				log.Errorf("clear %s tunnel data: %s", c.Client, err)
			}
		}
	}

	return nil
}

func clearTunnelData(client string) error {

	// client name format: xdc.eid.pid
	// This needs to be in k8s service name format so we can contact the xdcd service
	// and ask it to clean up the xdc attachment inside the container.
	srv := strings.ReplaceAll(client, ".", "-")

	log.Infof("clearing tunnel data on %s", srv)

	// the xdc service needs the service name and k8s namespace, which is "xdc". Example:
	// "xdc-hi-glawler.xdc:8321" is the address for the xdc "xdc" in the experiment "hi" in the
	// project "glawler".
	conn, err := grpc.Dial(
		srv+".xdc:8321", // should be exported from xdcd not hardcoded.
		grpc.WithInsecure(),
	)
	if err != nil {
		return fmt.Errorf("grpc dial: %w", err)
	}
	defer conn.Close()

	cli := xdcd.NewXdcdClient(conn)

	_, err = cli.ClearTunnelData(
		context.Background(),
		&xdcd.ClearTunnelDataRequest{},
	)
	if err != nil {
		return fmt.Errorf("clear tunnel: %w", err)
	}

	return nil
}
