package main

import (
	"flag"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/wgd/common"
)

func usage() {
	fmt.Println("wgdcood --listenon addr:port")
}

var xdcCert string

func main() {
	listenon := flag.String("listenon", "0.0.0.0:6000", "Where and how to listen as a daemon.")
	cert := flag.String("cert", "/etc/wgdcoord/wgdcoord.pem", "Path to the wgdcoord cert")
	key := flag.String("key", "/etc/wgdcoord/wgdcoord-key.pem", "Path to the wgdcoord private key")
	xdcCert = *flag.String("xdccert", "/etc/wgdcoord/worker-wgd.pem", "Path to the cert of the wgd for XDCs")
	verbose := flag.Bool("verbose", false, "If set, show debug logging.")
	version := flag.Bool("version", false, "Show version and exit.")

	flag.Parse()

	if *version == true {
		fmt.Println(common.Version)
		os.Exit(0)
	}

	if _, err := os.Stat(xdcCert); os.IsNotExist(err) {
		fmt.Printf("XDC Cert path/file does not exist: %s\n", xdcCert)
		os.Exit(668)
	}

	if *verbose == true {
		log.SetLevel(log.DebugLevel)
	}

	// run as a daemon
	err := Daemonize(*listenon, *cert, *key)
	if err != nil {
		fmt.Println("Error starting daemon: ", err)
		os.Exit(666)
	}
	os.Exit(0)
}
