package main

import (
	"gitlab.com/mergetb/engine/mfs"
	"net"
	"testing"
)

func setupEnclaveInMFS(enclaveid string) error {

	// setup the enclave address pool
	wga := &WgEnclaveAddress{Enclaveid: enclaveid}
	err := mfs.GetObject(wga)
	if err != nil {
		return err
	}
	mask := net.IPv4Mask(255, 255, 255, 0)                 // ffffff00 - GTL this should not be hardcoded.
	wga.Mask = [4]byte{mask[0], mask[1], mask[2], mask[3]} // better way to do this, I'm sure.
	ip, _, err := net.ParseCIDR("172.30.0.0/16")
	ip = ip.To4()
	ip[2] = 1 // Again, should not be hardcoded. We take x.x.1.0/24 for wireguard addresses
	wga.Addr = [4]byte{ip[0], ip[1], ip[2], ip[3]}
	_, err = mfs.PutObject(wga)
	return err
}

func teardownEnclave(enclaveid string) error {
	wge := &WgEnclave{Enclaveid: enclaveid}
	err := mfs.GetObject(wge)
	if err != nil {
		return err
	}
	_, err = mfs.DelObject(wge)
	return err
}

func TestAddressReservation(t *testing.T) {
	enclaveid := "thisisprobablynotarealenclave"
	key := "thisisafakekey"
	err := setupEnclaveInMFS(enclaveid)
	if err != nil {
		t.Fatal(err)
	}
	defer teardownEnclave(enclaveid)

	for i := 0; i < 10; i++ {
		a, err := ReserveEnclaveAddress(enclaveid)
		t.Logf("reserved: %s", a)

		err = ClaimEnclaveAddress(enclaveid, a, key)
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("claimed: %s", a)

		err = FreeEnclaveAddress(enclaveid, key)
	}
}
