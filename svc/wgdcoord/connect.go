package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/tech/wgd/api/wgd"
)

func WgdClient(ep, cert string) (*grpc.ClientConn, wgd.WgdClient, error) {
	log.Printf("wgd grpc connect to %s", ep)

	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM([]byte(cert)) {
		return nil, nil, fmt.Errorf("failed to append cert")
	}
	host, _, err := net.SplitHostPort(ep)
	if err != nil {
		return nil, nil, fmt.Errorf("bad endpoint")
	}
	log.Infof("connecting to %s with cert for %s", ep, host)
	creds := credentials.NewTLS(&tls.Config{
		ServerName:         host,
		RootCAs:            cp,
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial(
		fmt.Sprintf("%s", ep),
		grpc.WithTransportCredentials(creds),
	)
	if err != nil {
		log.WithFields(log.Fields{
			"ep":  ep,
			"err": err,
		}).Error("bad connect")
		return nil, nil, fmt.Errorf("error connecting to wgd")
	}

	return conn, wgd.NewWgdClient(conn), nil
}

// Wgd executes a function in the context of a client wgd grpc connection.
func Wgd(ep, cert string, f func(wgd.WgdClient) error) error {
	conn, cli, err := WgdClient(ep, cert)
	if err != nil {
		return err
	}
	defer conn.Close()
	return f(cli)
}
