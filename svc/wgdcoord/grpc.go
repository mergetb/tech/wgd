package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/wgd/api/wgdcoord"
	"gitlab.com/mergetb/tech/wgd/common"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"net"
)

//
// grpc boilerplate
//

// Server holds grpc functions.
type Server struct{}

// Daemonize runs the server and never returns. Listenon is the addr:port to
// listen for messages on.
func Daemonize(listenon, cert, key string) error {
	log.Infof("Starting daemon mode. Version %s", common.Version)

	creds, err := credentials.NewServerTLSFromFile(cert, key)
	if err != nil {
		log.WithError(err).Fatal("failed to load credentials")
	}
	grpcServer := grpc.NewServer(grpc.Creds(creds))
	api.RegisterWgdcoordServer(grpcServer, &Server{})

	l, err := net.Listen("tcp", listenon)
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
		return err
	}
	log.Infof("listening on tcp://%s", listenon)
	grpcServer.Serve(l)
	return nil
}

// CreateEnclave ...
func (s *Server) CreateEnclave(
	ctx context.Context, req *api.CreateEnclaveReq,
) (*api.CreateEnclaveResp, error) {
	l := log.WithFields(log.Fields{
		"enclaveid":  req.Enclaveid,
		"allowedips": req.Allowedips,
		"gwep":       req.Gatewayendpoint,
	})
	l.Info("create enclave")

	err := DoCreateEnclave(req.Enclaveid, req.Allowedips, req.Gatewayendpoint, req.Gatewaycert)
	if err != nil {
		return nil, err
	}
	return &api.CreateEnclaveResp{
		Enclaveid: req.Enclaveid,
		Success:   true,
	}, nil
}

// DeleteEnclave ...
func (s *Server) DeleteEnclave(
	ctx context.Context, req *api.DeleteEnclaveReq,
) (*api.DeleteEnclaveResp, error) {
	l := log.WithFields(log.Fields{
		"enclaveid": req.Enclaveid,
	})
	l.Info("delete enclave")

	ok, err := DoDeleteEnclave(req.Enclaveid)
	if err != nil {
		return nil, err
	}
	return &api.DeleteEnclaveResp{
		Enclaveid: req.Enclaveid,
		Success:   ok,
	}, nil
}

// ClientAttach ....
func (s *Server) ClientAttach(
	ctx context.Context, req *api.ClientAttachReq,
) (*api.ClientAttachResp, error) {

	l := log.WithFields(log.Fields{
		"enclaveid": req.Enclaveid,
		"publickey": req.Publickey,
	})
	l.Info("ClientAttach")

	key, endpoint, allowedips, addr, err := DoClientAttach(req.Enclaveid, req.Publickey)
	if err != nil {
		l.Errorf("client connect: %v", err)
		return nil, err
	}
	return &api.ClientAttachResp{
		Enclaveid:      req.Enclaveid,
		Enclaveaddress: addr,
		Gatewaykey:     key,
		Gatewayep:      endpoint,
		Allowedips:     allowedips,
	}, nil
}

// ClientDetach ....
func (s *Server) ClientDetach(
	ctx context.Context, req *api.ClientDetachReq,
) (*api.ClientDetachResp, error) {

	l := log.WithFields(log.Fields{
		"enclaveid":   req.Enclaveid,
		"publickey":   req.Publickey,
		"enclaveaddr": req.Enclaveaddress,
	})
	l.Info("ClientDetach")

	ok := true
	err := DoClientDetach(req.Enclaveid, req.Publickey, req.Enclaveaddress)
	if err != nil {
		l.Errorf("client detach %v", err)
		ok = false
	}
	return &api.ClientDetachResp{Success: ok}, nil
}

//
// Gateway attach/detach Not supported at the moment.
//
// // GatewayAttach resets the gateway peer information.
// func (s *Server) GatewayAttach(
// 	ctx context.Context, req *api.GatewayAttachReq,
// ) (*api.GatewayAttachResp, error) {
//
// 	localLog := log.WithFields(log.Fields{
// 		"enclaveid":  req.Enclaveid,
// 		"serverkey":  req.Serverkey,
// 		"endpoint":   req.Endpoint,
// 		"allowedips": req.Allowedips,
// 	})
// 	localLog.Info("GatewayAttach")
//
// 	peers, err := DoGatewayAttach(req.Enclaveid, req.Serverkey, req.Endpoint, req.Allowedips)
// 	if err != nil {
// 		localLog.Errorf("server connect: %v", err)
// 		return nil, err
// 	}
// 	return &api.GatewayAttachResp{
// 		Success: true,
// 		Peers: peers,
// 	}, nil
// }
//
// // ServerDisconnect ...
// func (s *Server) ServerDisconnect(
// 	ctx context.Context, scr *api.ServerDisconnectRequest,
// ) (*api.ServerDisconnectResponse, error) {
//
// 	localLog := log.WithFields(log.Fields{
// 		"wgcid": scr.Wgcid,
// 	})
// 	localLog.Info("ServerDisconnect")
//
// 	success, err := DoServerDisconnect(scr.Wgcid)
// 	if err != nil {
// 		localLog.Errorf("server disconnect: %v", err)
// 		return nil, err
// 	}
// 	return &api.ServerDisconnectResponse{Success: success}, nil
// }

// ClientContainerCreate ...
func (s *Server) ClientContainerCreate(
	ctx context.Context, req *api.ClientContainerCreateReq,
) (*api.ClientContainerCreateResp, error) {

	localLog := log.WithFields(log.Fields{
		"enclaveid":     req.Enclaveid,
		"containername": req.Containername,
	})
	localLog.Info("ClientContainerCreate")

	key, encaddr, err := DoClientContainerCreate(req.Enclaveid, req.Containername)
	if err != nil {
		localLog.Errorf("container create disconnect: %v", err)
		return nil, err
	}
	return &api.ClientContainerCreateResp{
		Enclaveid:      req.Enclaveid,
		Success:        true,
		Enclaveaddress: encaddr,
		Publickey:      key,
	}, nil
}

// ClientContainerDelete ...
func (s *Server) ClientContainerDelete(
	ctx context.Context, req *api.ClientContainerDeleteReq,
) (*api.ClientContainerDeleteResp, error) {

	localLog := log.WithFields(log.Fields{
		"enclaveid":     req.Enclaveid,
		"containername": req.Containername,
	})
	localLog.Info("ClientContainerDelete")

	_, err := DoClientContainerDelete(req.Enclaveid, req.Containername)
	if err != nil {
		localLog.Errorf("container create disconnect: %v", err)
		return nil, err
	}
	return &api.ClientContainerDeleteResp{
		Enclaveid: req.Enclaveid,
		Success:   true,
	}, nil
}

// ClientContainerStatus ...
func (s *Server) ClientContainerStatus(
	ctx context.Context, req *api.ClientContainerStatusReq,
) (*api.ClientContainerStatusResp, error) {

	localLog := log.WithFields(log.Fields{
		"containername": req.Containername,
	})
	localLog.Info("ClientContainerStatus")

	attached, encid, status, err := DoClientContainerStatus(req.Containername)
	if err != nil {
		localLog.Errorf("container status: %v", err)
		return nil, err
	}
	return &api.ClientContainerStatusResp{
		Attached:  attached,
		Enclaveid: encid,
		Wgstatus:  status,
	}, nil
}
