package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
)

// WgEnclave ...
type WgEnclave struct {
	// wireguard configuration
	Enclaveid       string // uniquely IDs this setof wg interfaces / network.
	GatewayKey      string
	GatewayEndpoint string // endpoint of wg interface itself.
	AllowedIPs      string // net.IPNet
	GatewayAddress  string // the address on the interface.

	ClientKeys []string

	// wgd configuration
	GatewayWgdCert     string
	GatewayWgdEndpoint string // endpoint of wgd listening on site.

	// internal
	Ver int64
}

// Key ...
func (wge *WgEnclave) Key() string {
	return fmt.Sprintf("/wgdcoord/%s", wge.Enclaveid)
}

// GetVersion ...
func (wge *WgEnclave) GetVersion() int64 {
	return wge.Ver
}

// SetVersion ...
func (wge *WgEnclave) SetVersion(v int64) {
	wge.Ver = v
}

// Object ...
func (wge *WgEnclave) Object() interface{} {
	return wge
}

// Merge ...
func (wge *WgEnclave) Merge(other interface{}) error {

	log.Infof("merging %v and %v", wge, other)
	o, ok := other.(*WgEnclave)
	if !ok {
		log.Errorf("other object is not a WgEnclave")
		return fmt.Errorf("other object is not a WgEnclave")
	}
	if wge.Key() != o.Key() {
		log.Errorf("other WgEnclave has different key")
		return fmt.Errorf("other WgEnclave has different key")
	}

	wge.Enclaveid = o.Enclaveid

	// new values overwrite old values!
	if o.GatewayKey != "" {
		wge.GatewayKey = o.GatewayKey
	}
	if o.GatewayEndpoint != "" {
		wge.GatewayEndpoint = o.GatewayEndpoint
	}
	if o.AllowedIPs != "" {
		wge.AllowedIPs = o.AllowedIPs
	}

	// Merge client keys, if needed.
	wge.ClientKeys = union(wge.ClientKeys, o.ClientKeys)

	return nil
}

func union(a, b []string) []string {
	m := make(map[string]bool)

	for _, item := range append(a, b...) {
		m[item] = true
	}
	var result []string
	for item := range m {
		result = append(result, item)
	}
	return result
}

type WgEnclaveAddress struct {
	Enclaveid string
	Mask      [4]byte
	Addr      [4]byte
	InUse     [256]bool

	// internal
	Ver int64
}

func (wga *WgEnclaveAddress) Key() string {
	return fmt.Sprintf("/wgdaddress/%s", wga.Enclaveid)
}

// GetVersion ...
func (wga *WgEnclaveAddress) GetVersion() int64 {
	return wga.Ver
}

// SetVersion ...
func (wga *WgEnclaveAddress) SetVersion(v int64) {
	wga.Ver = v
}

// Object ...
func (wga *WgEnclaveAddress) Object() interface{} {
	return wga
}

func (wga *WgEnclaveAddress) Merge(other interface{}) error {
	return nil
}

// Keep track of who has which key.
type AddressReservation struct {
	Enclaveid string
	Address   string
	ID        string

	// internal
	Ver int64
}

func (ar *AddressReservation) Key() string {
	return fmt.Sprintf("/wgaddrresv/%s/%s", ar.Enclaveid, ar.ID)
}

// GetVersion ...
func (ar *AddressReservation) GetVersion() int64 {
	return ar.Ver
}

// SetVersion ...
func (ar *AddressReservation) SetVersion(v int64) {
	ar.Ver = v
}

// Object ...
func (ar *AddressReservation) Object() interface{} {
	return ar
}

func (ar *AddressReservation) Merge(other interface{}) error {
	return nil
}

// client id to enclave id mapping.
type EnclaveClient struct {
	Client    string
	Enclaveid string

	// internal
	Ver int64
}

func (ec *EnclaveClient) Key() string {
	return fmt.Sprintf("/wgclient/%s", ec.Client)
}

// GetVersion ...
func (ec *EnclaveClient) GetVersion() int64 {
	return ec.Ver
}

// SetVersion ...
func (ec *EnclaveClient) SetVersion(v int64) {
	ec.Ver = v
}

// Object ...
func (ec *EnclaveClient) Object() interface{} {
	return ec
}

func (ec *EnclaveClient) Merge(other interface{}) error {
	return nil
}
