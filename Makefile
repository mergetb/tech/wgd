prefix ?= /usr/local

.PHONY: all
all: build/wgd build/wgdcoord build/wgdctl

.PHONY: containers
containers: wgdcoord-ctr

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/tech/wgd/common.Version=$(VERSION)"

build/wgd: svc/wgd/*.go api/wgd/spec.pb.go | build
	$(go-build)

build/wgdcoord: svc/wgdcoord/*.go api/wgdcoord/spec.pb.go | build
	$(go-build)

build/wgdctl: svc/wgdctl/*.go api/wgdcoord/spec.pb.go api/wgd/spec.pb.go | build
	$(go-build)


api/%/spec.pb.go: api/%/spec.proto
	$(protoc-build)

%-ctr: containers/%.dock build/%
	$(call docker-build,$(subst -ctr,,$(@)))

build:
	$(QUIET) mkdir -p build

tools: .tools/protoc-gen-go

.tools/protoc-gen-go: .tools
	GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go

.tools:
	mkdir .tools

.PHONY: clean
clean:
	$(QUIET) rm -rf build/wgd*

.PHONY: distclean
distclean: clean
	$(QUIET) $(RM) -rf .tools
	$(QUIET) find api -name "*.pb.go" -delete

.PHONY: install
install: build/wgd
	install -D $< $(DESTDIR)$(prefix)/bin/wgd

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

REGISTRY ?= quay.io
TAG ?= latest
REGPORT ?=
ORG ?= mergetb

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define docker-build
	$(call build-slug,docker)
	$(QUIET) docker build \
		${BUILD_ARGS} $(DOCKER_QUIET) -f $< \
		-t $(REGISTRY)$(REGPORT)/$(ORG)/$1:$(TAG) .
	$(if ${PUSH},$(call docker-push,$1))
endef

define docker-push
	$(call build-slug,push)
	$(QUIET) docker push $(REGISTRY)$(REGPORT)/$(ORG)/$1:$(TAG)
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=${LDFLAGS} -o $@ $(dir $<)*.go
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I . \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
endef
