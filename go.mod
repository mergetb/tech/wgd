module gitlab.com/mergetb/tech/wgd

go 1.13

require (
	github.com/coreos/etcd v3.3.13+incompatible // indirect
	github.com/golang/protobuf v1.3.5
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	gitlab.com/mergetb/engine v0.5.6
	gitlab.com/mergetb/portal/xdcd v0.0.0-20200829013402-db9fb94af481 // indirect
	go.etcd.io/etcd v0.5.0-alpha.5.0.20200306183522-221f0cc107cb // indirect
	google.golang.org/grpc v1.31.1
	gopkg.in/yaml.v2 v2.2.2
	k8s.io/apimachinery v0.0.0-20190223094358-dcb391cde5ca
	k8s.io/client-go v10.0.0+incompatible
)

// etcd is broken wrt to modules. force the issue here.
replace google.golang.org/grpc => google.golang.org/grpc v1.27.1
