This repository is a system for creating, destroying, and configuring sets of wireguard connected enclaves.

There are three entities in the system:

1) `wgd` is a daemon which runs on end user systems and is responsible for creating wireguard interfaces and configuring them correctly. There is a `wcd` daemon per machine in the enclave. 
2) `wgdcoord` is a daemon which coordinates wireguard information among members of the enclave. There is one of these.
3) `wcdctl` is a command line interface for using the system. In a deployed environment this may be used to register or unregister members of the enclave "by hand". 

All communication between the entities is via a GRPC API.

There are three parts or three sub-protocols handled by the wgd system: 1) initializing a wireguard enclave, 2) Creating wireguard interfaces on hosts, and 3) Key distribution.

1) Initializating a wireguard enclave. A wg enclave consists of a group of wireguard interfaces that share a network address space and a unique wireguard enclave ID.  An enclave is assumed to have a "gateway" behind which most of not all of the network address space lives. Then a set of "clients" which have a single address in the space. An enclave is initialized with an address space CIDR, a gateway endpoint (and a wgd running at that endpoint), and system-wide unique enclave ID. 

2) Wireguard daemons (wgd) have an API that allows external entities to create a wiregaurd interface on a host. This interface has the following properties:

    - a private and public key. this is generated on the host and given to the wgd coordinator (wgdcoord). 
    - a port that it listens on for encrypted packets
    - a publically accessible endpoint
    - a set of peer from which it accepts packets

3) A wireguard interface can join and leave an enclave. To join an enclave, it sends its peer information (public key) and gets back the gateway's peer information (key, endpoint, allowed-ips (full address space)) and an address it can use in the wireguard tunnel to access the enclave address space. It sets its wireguard interface to this address and sets the gateway as a peer.

Join protocol:

1) the client send public key and an enclave ID to wgdcoord
2) the wgdcoord responds with an access address and the gateway peer information (public key, allowed-ips, endpoint)
3) the client adds the gateway peer information to its wireguard interface and sets the interface address to the access address given
4) the wgcoord sends the client public key to teh wgd instace on the gateway. the gateway wgd adds this key as a peer to the given enclave wiregaurd interface with the access ip as the allowed-ips.

Leave protocol:

1) client sends its public key and enclave ID to the wgdcoord.
2) wgdcoord frees the access address of the client to be used by another client and removes the client information from its data store
3) wgdcoord sends a delete-peer message with the enclave id and public key to the gateway wgd. 
4) the gateway wgd removes the client's peer entry from the enclave wireguard interface.
