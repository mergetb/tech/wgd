#!/bin/bash

set -e

if [[ $# -ne 1 ]]; then
    echo "build-deb.sh [stable|testing]"
    exit 1
fi

rm -f debian
ln -s deb/$1 debian

build=build/deb/$1
rm -rf $build

debuild \
    --build-hook="make tools" \
    -e V=1 -e prefix=/usr -e arch=amd64 \
    $DEBUILD_ARGS -aamd64 -i -us -uc -b

mkdir -p $build
mv ../wgd*.build* $build/
mv ../wgd*.changes $build/
mv ../wgd*.deb $build/

rm debian

