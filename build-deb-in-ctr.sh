#!/bin/bash

set -e

docker build -f debian/builder.dock -t wgd-builder .
docker run -v `pwd`:/wgd wgd-builder /wgd/build-deb.sh
